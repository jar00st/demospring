package com.example.demo.services;

import com.example.demo.interfaces.UserRepository;
import com.example.demo.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServices {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProfileServices profileServices;

    public User createUser(User user) {
        return userRepository.save(user);
    }

    public List<User> createUserList(List<User> list) {
        return (List<User>) userRepository.saveAll(list);
    }

    public List<User> getUserList() {
        return (List<User>) userRepository.findAll();
    }

    public User getUserById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    public User updateUserById(User user) {
        Optional<User> userFound = userRepository.findById(user.getId());

        if (userFound.isPresent()) {
            User userUpdate = userFound.get();
            userUpdate.setName(user.getName());
            userUpdate.setEmail(user.getEmail());
            userUpdate.setPassword(user.getPassword());
//            System.out.println(user.toString());
            userUpdate.setProfile(user.getProfile());


            return userRepository.save(user);
        } else {
            return null;
        }
    }

    public String deleteUserById(Long id) {
        userRepository.deleteById(id);
        return "User "+ id +" deleted";
    }
}

package com.example.demo.services;

import com.example.demo.interfaces.ProfileRepository;
import com.example.demo.models.Profile;
import com.example.demo.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProfileServices {
    @Autowired
    private ProfileRepository profileRepository;

    public Profile createProfile(Profile profile){
        return profileRepository.save(profile);
    }
    public List<Profile> createProfileList(List<Profile> profiles){
        return (List<Profile>) profileRepository.saveAll(profiles);
    }
    public List<Profile> getProfileList(){
        return (List<Profile>) profileRepository.findAll();
    }
    public Profile getProfileById(Long id) {
        return profileRepository.findById(id).orElse(null);
    }
    public Profile updateProfileById(Profile profile) {
        Optional<Profile> profileFound = profileRepository.findById(profile.getId());

        if (profileFound.isPresent()) {
            Profile profileUpdate = profileFound.get();
            profileUpdate.setName(profile.getName());
            profileUpdate.setRoutes(profile.getRoutes());


            return profileRepository.save(profile);
        } else {
            return null;
        }
    }

    public String deleteProfileById(Long id) {
        profileRepository.deleteById(id);
        return "Profile "+ id +" deleted";
    }
}

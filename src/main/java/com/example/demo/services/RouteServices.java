package com.example.demo.services;

import com.example.demo.interfaces.RouteRepository;
import com.example.demo.models.Profile;
import com.example.demo.models.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RouteServices {

    @Autowired
    private RouteRepository routeRepository;
    public Route createRoute(Route route){
        return routeRepository.save(route);
    }
    public List<Route> createRouteList(List<Route> routes){
        return (List<Route>) routeRepository.saveAll(routes);
    }
    public List<Route> getRouteList(){
        return (List<Route>) routeRepository.findAll();
    }
    public Route getRouteById(Long id) {
        return routeRepository.findById(id).orElse(null);
    }
    public Route updateRouteById(Route route) {
        Optional<Route> profileFound = routeRepository.findById(route.getId());

        if (profileFound.isPresent()) {
            Route routeUpdate = profileFound.get();
            routeUpdate.setName(route.getName());
            routeUpdate.setStr(route.getStr());
            routeUpdate.setProfiles(route.getProfiles());


            return routeRepository.save(route);
        } else {
            return null;
        }
    }

    public String deleteRouteById(Long id) {
        routeRepository.deleteById(id);
        return "Route "+ id +" deleted";
    }
}

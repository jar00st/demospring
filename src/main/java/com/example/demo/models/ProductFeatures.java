package com.example.demo.models;

import javax.persistence.*;

@Entity
@Table(name="product_features")
public class ProductFeatures {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

}

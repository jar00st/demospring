package com.example.demo.models;

import javax.persistence.*;

@Entity
@Table(name="products")
public class Product {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;
    @Column
    private String str;

    @OneToOne
    @JoinColumn(name = "tag_id")
    private ProductTag tag;
}

package com.example.demo.models;

import javax.persistence.*;

@Entity
@Table(name="types")
public class Type {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;
}

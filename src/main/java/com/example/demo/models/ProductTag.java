package com.example.demo.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="product_tags")
public class ProductTag {
    @Column
    private String name;
    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    @JoinColumn(name = "father_id")
    private ProductTag father_id;

    @OneToMany(mappedBy = "father_id" ,cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<ProductTag> children;
}

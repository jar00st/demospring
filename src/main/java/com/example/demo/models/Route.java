package com.example.demo.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="routes")
public class Route {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;
    @Column
    private String str;

    @ManyToMany(mappedBy = "routes", fetch = FetchType.LAZY)
    private Set<Profile> profiles = new HashSet<>();

    public Route() {
    }

    public Route(String name, String str, Set<Profile> profiles) {
        this.name = name;
        this.str = str;
        this.profiles = profiles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public Set<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(Set<Profile> profiles) {
        this.profiles = profiles;
    }
}

package com.example.demo.controllers;

import com.example.demo.models.Profile;
import com.example.demo.models.Route;
import com.example.demo.services.RouteServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@ComponentScan(basePackages={"com.example"})
@RequestMapping("/routes")
public class RouteController {
    @Autowired
    private RouteServices routeServices;
    @GetMapping("/")//you can give this any name you want and after adding this string to the end of base url you can use this
    public ResponseEntity<List<Route>> getAllUsers() {
        return ResponseEntity.ok(routeServices.getRouteList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Route> getRouteById(@PathVariable Long id) {
        return ResponseEntity.ok().body(this.routeServices.getRouteById(id));
    }

    @PostMapping("/addRoute")
    public ResponseEntity<Route> addRoute(@RequestBody Route profile) {
        System.out.println(profile.toString());
        return ResponseEntity.ok(this.routeServices.createRoute(profile));
    }

    @PostMapping("/addRoutes")
    public ResponseEntity<List<Route>> addRoute(@RequestBody List<Route> list) {
        return ResponseEntity.ok(this.routeServices.createRouteList(list));
    }

    @PutMapping("/updateProfiles/")
    public ResponseEntity<Route> updateRoute(@RequestBody Route profile) {
        return ResponseEntity.ok().body(this.routeServices.updateRouteById(profile));
    }

    @DeleteMapping("/deleteRoutes/{id}")
    public HttpStatus deleteRoute(@PathVariable Long id) {
        this.routeServices.deleteRouteById(id);
        return HttpStatus.OK;
    }
}

package com.example.demo.controllers;

import com.example.demo.models.Profile;
import com.example.demo.models.User;
import com.example.demo.services.ProfileServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@ComponentScan(basePackages={"com.example"})
@RequestMapping("/profiles")
public class ProfileController {
    @Autowired
    private ProfileServices profileServices;
    @GetMapping("/")//you can give this any name you want and after adding this string to the end of base url you can use this
    public ResponseEntity<List<Profile>> getAllUsers() {
        return ResponseEntity.ok(profileServices.getProfileList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Profile> getProfileById(@PathVariable Long id) {
        return ResponseEntity.ok().body(this.profileServices.getProfileById(id));
    }

    @PostMapping("/addProfile")
    public ResponseEntity<Profile> addProfile(@RequestBody Profile profile) {
        System.out.println(profile.toString());
        return ResponseEntity.ok(this.profileServices.createProfile(profile));
    }

    @PostMapping("/addProfiles")
    public ResponseEntity<List<Profile>> addProfile(@RequestBody List<Profile> list) {
        return ResponseEntity.ok(this.profileServices.createProfileList(list));
    }

    @PutMapping("/updateProfiles/")
    public ResponseEntity<Profile> updateProfile(@RequestBody Profile profile) {
        return ResponseEntity.ok().body(this.profileServices.updateProfileById(profile));
    }

    @DeleteMapping("/deleteProfiles/{id}")
    public HttpStatus deleteProfile(@PathVariable Long id) {
        this.profileServices.deleteProfileById(id);
        return HttpStatus.OK;
    }
}

package com.example.demo.controllers;

import com.example.demo.interfaces.UserRepository;
import com.example.demo.models.User;
import com.example.demo.services.ProfileServices;
import com.example.demo.services.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@ComponentScan(basePackages={"com.example"})
@RequestMapping("/api")
public class UserController {
    @Autowired
    private UserServices services;
    @Autowired
    private ProfileServices profileServices;

    @GetMapping("/users")//you can give this any name you want and after adding this string to the end of base url you can use this
    public ResponseEntity<List<User>> getAllUsers() {
        return ResponseEntity.ok(services.getUserList());
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Long id) {
        return ResponseEntity.ok().body(this.services.getUserById(id));
    }

    @PostMapping("/addUser")
    public ResponseEntity<User> addUser(@RequestBody User user) {
//        System.out.println(profileServices.getProfileById(user.getTemp_profile_id()));
        user.setProfile(profileServices.getProfileById(user.getTemp_profile_id()));
        return ResponseEntity.ok(this.services.createUser(user));
    }

    @PostMapping("/addUsers")
    public ResponseEntity<List<User>> addUsers(@RequestBody List<User> list) {

        return ResponseEntity.ok(this.services.createUserList(list));
    }

    @PutMapping("/updateUsers/")
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        user.setProfile(profileServices.getProfileById(user.getTemp_profile_id()));
        return ResponseEntity.ok().body(this.services.updateUserById(user));
    }

    @DeleteMapping("/deleteUsers/{id}")
    public HttpStatus deleteUser(@PathVariable Long id) {
        this.services.deleteUserById(id);
        return HttpStatus.OK;
    }
}

package com.example.demo.interfaces;

import com.example.demo.models.Route;
import org.springframework.data.repository.CrudRepository;

public interface RouteRepository extends CrudRepository<Route, Long> {
}
